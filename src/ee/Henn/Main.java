package ee.Henn;

import java.sql.Connection;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.DriverManager;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        String hostName = "hennusql.database.windows.net"; // update me
        String dbName = "Northwind"; // update me
        String user = "Student"; // update me
        String password = "Pa$$w0rd"; // update me
        String url = String.format("jdbc:sqlserver://%s:1433;database=%s;user=%s;password=%s;encrypt=true;"
                + "hostNameInCertificate=*.database.windows.net;loginTimeout=30;", hostName, dbName, user, password);

        try (Connection connection = DriverManager.getConnection(url)) {

            Scanner scan = new Scanner(System.in);
            System.out.print("Anna tootekood: ");
            int productid = scan.nextInt();
            System.out.print("Anna uus hind: ");
            double unitprice = scan.nextDouble();


            String updateSql =

                    "update products set unitprice = ? where productid = ?"

                    ;


            String selectSql = "select CategoryName, ProductName, UnitPrice "
                    + "FROM Products p "
                    + "JOIN Categories c ON p.categoryid = c.categoryid";

            Statement update = connection.prepareStatement(updateSql);
            ((PreparedStatement) update).setDouble(1, unitprice);
            ((PreparedStatement) update).setInt(2, productid);
            var kinnitus = update.executeUpdate(updateSql);


            try (Statement statement = connection.createStatement();

                 ResultSet resultSet = statement.executeQuery(selectSql)) {

                while(resultSet.next()) {
                    System.out.printf("%s %s %s%n",
                            resultSet.getString(1),
                            resultSet.getString(2),
                            resultSet.getDouble(3)

                    );
                }

            }

        } catch (Exception e) {
            System.out.println(e.getMessage());


        }

    }
}